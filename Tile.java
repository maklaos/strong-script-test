public class Tile {
    private int A;
    private int B;
    private float P;
    private float C;
    private float t;
    private float ka;
    private float kb;
    private int a;
    private int b;

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public void setA(int a) {
        A = a;
    }

    public void setB(int b) {
        B = b;
    }

    public void setP(float p) {
        P = p;
    }

    public void setC(float c) {
        C = c;
    }

    public void setT(float t) {
        this.t = t;
    }

    public void setKa(float ka) {
        this.ka = ka;
    }

    public void setKb(float kb) {
        this.kb = kb;
    }

    public void setAx(int a) {
        this.a = a;
    }

    public void setBx(int b) {
        this.b = b;
    }

    public float[][] heat() {
        float[][] tile = new float[A][B];

        for (int i = 0; i < tile.length; ++i) {
            for(int j = 0; j < tile[i].length; ++j) {
                tile[i][j] = temperature(i, j); //measure temperature
            }
        }

        return tile;
    }

    protected float temperature(int a1, int b1) {
        float T = 300; //room temperature
        if (a == a1 && b == b1) {
            if (ka > kb) {
                T += (this.P * this.C * this.t) / (kb - kb/ka);
            } else {
                T += (this.P * this.C * this.t) / (ka - ka/kb);
            }
        } else {
            T += (this.P * this.C * this.t) / (ka * Math.abs((a - a1)) + kb * Math.abs((b - b1)));
        }
        return T;
    }
}
