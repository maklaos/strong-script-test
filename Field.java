import javax.swing.*;
import java.awt.*;

public class Field extends JPanel {
    private float[][] distribution;
    private float maxTemperature;
    private int cellWidth;
    private int cellHeight;

    public Field(float[][] distribution, float maxTemperature, int cellWidth, int cellHeight) {
        this.distribution = distribution;
        this.maxTemperature = maxTemperature - 300;
        this.cellWidth = cellWidth;
        this.cellHeight = cellHeight;
    }

    public void paint(Graphics g) {

        for (int i = 0; i < distribution.length; i++) {
            for(int j = 0; j < distribution[i].length; j++) {
                int red = (int) (255 * ((distribution[i][j] - 300) / maxTemperature));
                g.setColor(new Color(red, 0, 0));
                g.fillRect(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
            }
        }

    }
}
