import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // get user inputs without validation
        Scanner sc = new Scanner(System.in);

        // create tile obj
        Tile tile = new Tile();

        System.out.println("Enter width of tile (int):");
        tile.setA(sc.nextInt());

        System.out.println("Enter height of tile (int):");
        tile.setB(sc.nextInt());

        System.out.println("Enter power:");
        tile.setP(sc.nextFloat());

        System.out.println("Enter specific heat capacity:");
        tile.setC(sc.nextFloat());

        System.out.println("Enter time (sec):");
        tile.setT(sc.nextFloat());

        System.out.println("Enter thermal conductivity along the side A:");
        tile.setKa(sc.nextFloat());

        System.out.println("Enter thermal conductivity along the side B:");
        tile.setKb(sc.nextFloat());

        System.out.println("Enter the (a) point of heater (int):");
        tile.setAx(sc.nextInt());

        System.out.println("Enter the (b) point of heater (int):");
        tile.setBx(sc.nextInt());
        
        // get tile temperature distribution
        float[][] distribution = tile.heat();

        // out tile temperature distribution
        for (int i = 0; i < distribution.length; i++) {
            for(int j = 0; j < distribution[i].length; j++) {
                System.out.print("[" + distribution[i][j] + "] ");
            }
            System.out.println();
        }

        Visual vs = new Visual();
        vs.view(distribution, tile.getA(), tile.getB());

    }
}
