import javax.swing.*;
import java.awt.*;

public class Visual {

    public void view(float[][] distribution, int a, int b) {
        int cellWidth = 20;
        int cellHeight = 20;

        JFrame frame = new JFrame(); // create frame
        Field field = new Field(distribution, distribution[a][b], cellWidth, cellHeight); // create draw field in frame

        int w = distribution.length * 20;
        int h = distribution[0].length * 20;

        frame.setBounds(0, 0, w, h);
        frame.setBackground(Color.GRAY);
        frame.setResizable(true);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(field);
    }
}
